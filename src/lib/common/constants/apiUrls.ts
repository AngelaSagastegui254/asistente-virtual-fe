export const BASE_URL = 'http://localhost:3000';
// export const BASE_URL = 'http://23.23.15.212';

export const apiUrls = {
    DOCTORS_URL: `/doctores`,
    PACIENTES_URL: `/pacientes`,
    RECETAS_MEDICAS_URL: `/recetas-medicas`,
};


